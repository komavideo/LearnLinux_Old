学习Linux系列
============

01. 一起安装Ubuntu
02. Ubuntu Server 网络设置
03. Ubuntu Server 系统更新
04. Ubuntu Server 文本编辑器
05. Ubuntu Server 包管理
06. Ubuntu Server 安装Web服务器（Apache）-(1)
07. Ubuntu Server 16 LTS 安装

## 课程文件

* http://git.oschina.net/komavideo/LearnLinux

## 小马视频频道（优酷）

http://i.youku.com/komavideo